package testcases;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class FlipKartShopping {

	//public static void main(String[] args) throws InterruptedException {
	@Test
	public void electronicsPageFlipkart() throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.get("https://www.flipkart.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.findElement(By.xpath("//button[text()='✕']")).click();
		Actions ac = new Actions(driver);
		ac.moveToElement(driver.findElement(By.xpath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]"))).perform();
		driver.findElement(By.linkText("Mi")).click();
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.titleContains("Mi"));
		String title = driver.getTitle();
		System.out.println(title);
		if (title.contains("Mi")) {
			System.out.println(title + " is Verified");
		}
			else {
				System.out.println(title + " is not as expected");
		}
			
		driver.findElement(By.xpath("//div[text()='Newest First']")).click();
		
		List<WebElement> lswebelements = driver.findElements(By.xpath("//div[@class='_3wU53n']"));
		
		List<String> lsstring = new ArrayList<String>();
		
		for (WebElement eachlswebelements : lswebelements ) {
			lsstring.add(eachlswebelements.getText());
			Thread.sleep(500);
			//System.out.println(lsstring);
		}
		
		
		List<WebElement> lsprice = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");
		
		List<String> lspricestring = new ArrayList<String>();
		
		for (WebElement eachlsprice : lsprice) {
			lspricestring.add(eachlsprice.getText());
			Thread.sleep(500);
			//System.out.println(lspricestring);
		}
			
			int size = lsstring.size();
			for (int i = 0; i < size-1; i++) {
				System.out.println(lsstring.get(i) + lspricestring.get(i));
			}
		}
		
		
		
		
		
		

	}


